<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosensTable extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosens', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nama');
            $table->string('nipd');

            $table->timestamps();
        });


        Schema::table('mahasiswas', function(Blueprint $table) {
			$table->foreign('id_dosen')->references('id')->on('dosens')->onDelete('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas', function(Blueprint $table) {
			$table->dropForeign('id_dosen');
        });

        Schema::dropIfExists('dosens');
    }
}